FROM maven:3.8.1-jdk-11-openj9
WORKDIR /app
COPY . .
RUN mvn package
EXPOSE 8080
CMD ["java", "-jar", "target/ter23-0.0.1-SNAPSHOT.jar"]
